*** Settings ***
Documentation       This test suite automates User Registration process.
...                 It contains the resource file which is in resource4UserRegistrationprocess.robot file
Library             SeleniumLibrary
Suite Setup         Set suite setup
Resource            ../Automations practice/resource4userregistration2.robot

Suite Teardown     close the browser

*** Test Cases ***
Verify mandatory fields empty error
    [Tags]     empty_mandatory_field
    Verify mandatory fields empty error

wrong username
    [Tags]      wrong_username
    wrong username       ..1Sabita     ..--+00       practicedemo16@gmail.com       @dmin123

user registration
    [Tags]      register
    user can register

Verify invalid email address error
     [Tags]    invalid_email
    Verify invalid email address error      practicedemo@gmail.com          @dmin123

ordering product
     [Tags]     order
    ordering product                        practicedemo16@gmail.com        @dmin123

user changes quantity validation
     [Tags]     updated_quantity
    user changes quantity validation

*** Keywords ***
Set suite setup
   open registration browser
   set selenium speed      ${delay}

verify mandatory fields empty error
    [Documentation]     This test suite verifies with the error message when registrating mandatory
    ...                 fields are left empty by user
    register
    select gender
    date of birth
    select newsletter
    registering
    error for mandatory

wrong username
    [Documentation]         This keyword tests validates the error messages when the user passess invalid firstname and/or lastname
    [Arguments]             ${firstname}    ${lastname}     ${email}    ${password}
    register
    select gender
    input firstname         ${firstname}
    input lastname          ${lastname}
    date of birth
    input email             ${email}
    input passwd            ${password}
    select newsletter
    input my password
    password confirmation
    registering

user can register
    [Documentation]     This test suite fills in the required fields and validates
    ...                 with the success message.
    register
    personal detail
    date of birth
    select newsletter
    input my password
    password confirmation
    registering
    validation

Verify invalid email address error
    [Documentation]     This test suite verifies with the message
    ...                 if invalid email is presented by user
    [Arguments]             ${email}     ${password}
    sign in
    input email             ${email}
    input passwd            ${password}
    log in
    error

ordering product
    [Documentation]     This test suite fills in all the required area
     ...                to order the desired product
    [Arguments]         ${email}       ${password}
    sign in
    input email         ${email}
    input passwd        ${password}
    log in
    scroll to apparel and select clothing
    scroll and select to running shirt
    select size
    select quantity
    add to cart
    selecting shopping cart
    discount code
    agree box
    checkout and validate
    country and city
    address
    postal code
    phone number
    continueing
    shipping method and continue
    payment method and continue
    payment information and continue
    confirm order


user changes quantity validation
    [Documentation]     This test suite updates the earlier ordered quantity and then
    ...                 fills in all the required areas to order the quantity.
    shop cart
    updating quantity

