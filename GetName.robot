*** Settings ***
Documentation       This test suite gets the name of the product
Library             SeleniumLibrary

*** Variables ***
${url}              http://automationpractice.com/
${browser}          chrome

*** Test Cases ***
get the product name
    opening my browser
    scroll to women and select T-shirts
    searching the same product name in search bar
    close my browser

*** Keywords ***
opening my browser
    open browser    ${url}      ${browser}

scroll to women and select T-shirts
    scroll element into view    xpath://*[@id="block_top_menu"]/ul/li[1]/a
    click element               xpath://*[@id="block_top_menu"]/ul/li[1]/ul/li[1]/ul/li[1]/a
    ${name}                     get text    xpath://*[@id="center_column"]/ul/li/div/div[2]/h5/a
    log to console              ${name}
close my browser
    close browser
searching the same product name in search bar
    input text                  name:search_query   Faded Short Sleeve T-shirts
    click element               xpath://header/div[3]/div[1]/div[1]/div[2]/form[1]/button[1]
    page should contain         Faded Short Sleeve T-shirts




