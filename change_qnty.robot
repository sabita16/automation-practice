
*** Settings ***
Documentation       This suite automates to select different categorizes and order the desired product.
...                 It contains the resource file which is in resource4userregistration2.robot file
Library             SeleniumLibrary
Suite Setup         open registration browser
Resource            ../Automations practice/resource4userregistration2.robot
Suite Teardown      close the browser
Test Template       ordering product


*** Test Cases ***
user info       practicedemo16@gmail.com        @dmin123

*** Keywords ***
ordering product
    sign in
    [Arguments]         ${email}       ${password}
    input email         ${email}
    input passwd        ${password}
    log in
    scroll to apparel and select clothing
    scroll and select to running shirt
    select size
    select quantity
    add to cart
    selecting shopping cart
    updating quantity
