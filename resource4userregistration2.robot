*** Settings ***
Documentation       This is a resource file which contains variables and collection of keywords
Library             SeleniumLibrary

*** Variables ***
${url}       https://demo.nopcommerce.com/
${browser}   chrome
${delay}     0.4 seconds


*** Keywords ***
open registration browser
    open browser            ${url}      ${browser}
    set selenium speed      ${delay}

register
    click element       xpath:/html/body/div[6]/div[1]/div[1]/div[2]/div[1]/ul/li[1]/a

personal detail
    select radio button       Gender        F
    input text          name:FirstName      Sabita
    input text          name:LastName       Manandhar
    input text          name:Email          practicedemo16@gmail.com

select gender
    select radio button       Gender        F

date of birth
    select from list by label   name:DateOfBirthDay      16
    select from list by label   name:DateOfBirthMonth    June
    select from list by label   name:DateOfBirthYear     1988

select newsletter
    click button            name:Newsletter

input my password
    input password          name:Password               @dmin123

password confirmation
    input password          name:ConfirmPassword        @dmin123

registering
    click button            xpath://*[@id="register-button"]

validation
    page should contain     Your registration completed

close the browser
    close browser

sign in
    click link          xpath:/html/body/div[6]/div[1]/div[1]/div[2]/div[1]/ul/li[2]/a

input email
    [Arguments]         ${email}
    input text          id:Email            ${email}

input passwd
    [Arguments]         ${password}
    input password      id:Password         ${password}

input firstname
    [Arguments]         ${firstname}
    input text          name:FirstName      ${firstname}

input lastname
    [Arguments]         ${lastname}
    input text          name:LastName       ${lastname}

log in
    click element               xpath:/html/body/div[6]/div[3]/div/div/div/div[2]/div[1]/div[2]/form/div[3]/button

error
    page should contain         Login was unsuccessful.

error for mandatory
    page should contain         First name is required.

scroll to apparel and select clothing
    scroll element into view    xpath:/html/body/div[6]/div[2]/ul[1]/li[3]/a
    click element               xpath:/html/body/div[6]/div[2]/ul[1]/li[3]/ul/li[2]/a

scroll and select to running shirt
    scroll element into view    xpath:/html/body/div[6]/div[3]/div/div[3]/div/div[2]/div[2]/div[2]/div/div/div[3]/div/div[1]/a/img
    click element               xpath:/html/body/div[6]/div[3]/div/div[3]/div/div[2]/div[2]/div[2]/div/div/div[3]/div/div[1]/a/img

select size
    select from list by label   name:product_attribute_11       1X

select quantity
    input text                  id:product_enteredQuantity_27      2

add to cart
    click button                xpath://*[@id="add-to-cart-button-27"]
    page should contain         The product has been added to your shopping cart

selecting shopping cart
    click element               xpath://*[@id="bar-notification"]/div/p/a

shop cart
    click element                xpath://*[@id="topcartlink"]/a/span[1]

gift wrapping
    select from list by label   checkout_attribute_label_1        2

discount code
    click element               xpath://*[@id="discountcouponcode"]
    input text                  xpath://*[@id="discountcouponcode"]     auri203
    click button                name:applydiscountcouponcode

agree box
    click element               xpath://*[@id="termsofservice"]

checkout and validate
    click button                name:checkout
    page should contain         Checkout

country and city
    select from list by label   name:BillingNewAddress.CountryId        Australia
    input text                  name:BillingNewAddress.City     Sydney

address
    input text                  name:BillingNewAddress.Address1     Sarbeshwor Marg

postal code
    input text                  name:BillingNewAddress.ZipPostalCode         345

Phone number
    input text                  name:BillingNewAddress.PhoneNumber      12345678

continueing
    click button                xpath://*[@id="billing-buttons-container"]/button[4]

shipping method and continue
    select radio button         shippingoption       Ground___Shipping.FixedByWeightByTotal
    click button                xpath://*[@id="shipping-method-buttons-container"]/button

payment method and continue
    select radio button         paymentmethod       Payments.CheckMoneyOrder
    click button                xpath://*[@id="payment-method-buttons-container"]/button

payment information and continue
    page should contain         Mail Personal or Business Check, Cashier's Check or money
    click button                xpath://*[@id="payment-info-buttons-container"]/button

confirm order
    click button                xpath://*[@id="confirm-order-buttons-container"]/button
    page should contain         Thank You

updating quantity
    input text                  xpath://*[@id="itemquantity11224"]    1
    click button                name:updatecart
    page should contain         Shopping cart (1)
    page should contain         $15.00

logo scroll to womens tshirt
    scroll element into view    xpath://*[@id="block_top_menu"]/ul/li[1]/a
    click element               xpath://*[@id="block_top_menu"]/ul/li[1]/ul/li[1]/ul/li[1]/a
    click element               xpath://*[@id="center_column"]/ul/li/div/div[1]/div/a[1]/img
    click element               xpath://*[@id="wishlist_button"]
    page should contain         You must be logged in to manage your wishlist.
    close browser


