
*** Settings ***
Documentation       This suite tests the invalid mandatory field by the user and validation
...                 It contains the resource file which is in resource4userregistration2.robot file
...                 Suite Setup and Suite Teardown are adjusted
Library             SeleniumLibrary
Suite Setup         open registration browser
Resource            ../Automations practice/resource4userregistration2.robot
Suite Teardown      close the browser

*** Test Cases ***
user registration process
    register
    date of birth
    select newsletter
    registering
    error for mandatory

