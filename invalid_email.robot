
*** Settings ***
Documentation       This suite verifies two test cases. The first one test the invalid email address error
...                 and the second one test the valid email.
...                 It contains the resource file which is in resource4userregistration2.robot file
...                 Suite Setup and Suite Teardown is adjusted
Library             SeleniumLibrary
Suite Setup         open registration browser
Resource            ../Automations practice/resource4userregistration2.robot
Suite Teardown      close the browser
Test Template       invalid email

*** Test Cases ***
invalid email       sabita@gmail.com            @dmin123
valid email         practicedemo16@gmail.com    @dmin123
*** Keywords ***
invalid email
    [Arguments]         ${email}    ${password}
    sign in
    input email         ${email}
    input passwd        ${password}
    log in
    error
