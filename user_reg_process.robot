*** Settings ***
Documentation       This test suite automates User Registration process.
...                 It contains the resource file which is in resource4UserRegistrationprocess.robot file
...                 Suite Setup and Suite Teardown is adjusted
Library             SeleniumLibrary
Suite Setup         open registration browser
Resource            ../Automations practice/resource4userregistration2.robot
Suite Teardown      close the browser

*** Test Cases ***
user can register
    [Documentation]     This test case passess the data in the given field and also validates with the message
    register
    personal detail
    date of birth
    select newsletter
    password confirmation
    registering
    validation


