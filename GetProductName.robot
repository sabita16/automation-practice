*** Settings ***
Documentation       This suite Automate 'Search Product' feature of e-commerce website with Selenium.
...                 It gets Name/Text of the first product displayed on the page.
...                 It enter the same product name in the search bar present on top of page and search it.
...                 It also validate that same product is displayed on searched page with same details which were displayed on T-Shirt's page.
...                 It contains the resource file which is in resource4userregistration2.robot file
Library             SeleniumLibrary
Suite Setup         open registration browser
Resource            ../Automations practice/resource4userregistration2.robot
Suite Teardown      close the browser

*** Test Cases ***
search product
    scroll to apparel and select clothing
    ${allLinks}      get element count    xpath://h1[contains(text(),'Clothing')]
    log to console      ${allLinks}

    @{names}     create list
    FOR  ${i}   IN RANGE    1       ${allLinks}
    \   ${producttext}      get text   (xpath://h1[contains(text(),'Clothing')])[${i}]
    \   log to console      ${producttext}
    END


