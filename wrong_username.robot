
*** Settings ***
Documentation       This test suite tests validates the error messages when the user passess invalid firstname and/or lastname
...                 It contains the resource file which is in resource4userregistration2.robot file
Library             SeleniumLibrary
Suite Setup         open registration browser
Resource            ../Automations practice/resource4userregistration2.robot
Suite Teardown      close the browser
Test Template       wrong entry


*** Test Cases ***
wrong entry         ..1Sabita     ..--+00       practicedemo16@gmail.com       @dmin

*** Keywords ***
wrong entry
    [Arguments]             ${firstname}    ${lastname}     ${email}    ${password}
    register
    select gender
    input firstname         ${firstname}
    input lastname          ${lastname}
    date of birth
    input email             ${email}
    input passwd            ${password}
    select newsletter
    input my password
    password confirmation
    registering


