
*** Settings ***
Documentation       This suite automates to select different categorizes and order the desired product.
...                 It contains the resource file which is in resource4userregistration2.robot file
Library             SeleniumLibrary
Resource            ../Automations practice/resource4userregistration2.robot

*** Variables ***
${mylogo_url}       http://automationpractice.com/index.php
${browser}          chrome

*** Test Cases ***
validating add to wishlist
    set selenium speed  ${delay}
    open browser    ${mylogo_url}   ${browser}
    logo scroll to womens tshirt
    close browser
